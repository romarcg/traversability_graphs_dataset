# Traversability graphs

Two heightmaps were designed to compute their traversability graph:

- `custom9` has clear obstacles (walls) and will help to identify straightforward paths
- `heightmap1` was generated using a sum of Perlin noise. Also, I purposely added two ramps so that the robot could avoid a wall.

Each directed graph has:

- `~((512-60)/stride)^2` nodes (nodes identifiers are strings from '0' to '`~((512-60)/stride)^2`')
- 8 edges per node connecting its 8 neighbours
- each edge has these properties:
  - `label`, with the *probability* of traversing from current node to the connected one (for visualization purposes)
  - `neighbour`, the index of the cell (the heightmap is a grid) of the respective neighbour
  - `probability`, *probability* of traversing from current node to the connected one
  - `weight`, 1-`probability`
  - `distance`, the distance in m between current node and respective neighbour (this distance depends on the position of the neightbour and on the stride)

## files

### `heightmaps`

Contains the grayscale images of the heightmaps (different sizes). When importing them to vrep consider that:

![](heightmaps/heightmap1.png)
> - size of the map = `10x10m`
> - image size = `512x512px`
> - resolution = `~1.9cm` per pixel
> - height scale = `1.0m`
> - stride = `7`

![](heightmaps/custom9.png)
> - size of the map = `10x10m`
> - image size = `512x512px`
> - resolution = `~1.9cm` per pixel
> - height scale = `1.0m`
> - stride = `7`

![](heightmaps/arc_rocks.png)
> - size of the map = `10x10m`
> - image size = `512x512px`
> - resolution = `~1.9cm` per pixel
> - height scale = `0.4m`
> - only full graph is available
> - stride = `7`

![](heightmaps/gridmap_elevation_2_c.png)
> - this is the heightmap from eth/asl's lab
> - image size = `212x212px`
> - size of the map = `6.36x6.36m`
> - resolution = `3cm` per pixel
> - height scale = `0.939174m`
> - patch size for estimation = `40x40px`
> - iteration started at `(20,20)px`
> - stride = `3`
> - only full graph is available

![](heightmaps/gridmap_elevation_2_c_r.png)
> - this is the heightmap from eth/asl's lab (re-scaled size and height)
> - image size = `475x475px`
> - size of the map = `9.5x9.5m`
> - resolution = `2cm` per pixel
> - height scale = `0.739174m`
> - patch size for estimation = `60x60px`
> - iteration started at `(30,30)px`
> - stride = `7`
> - only full graph is available

![](heightmaps/quarry_cropped4_scaled2cm.png)
> - this is the heightmap from the quarry
> - image size = `1600x1600px`
> - size of the map = `30.4x30.4m`
> - resolution = `2cm` per pixel
> - max height = `10m`
> - patch size for estimation = `60x60px`
> - iteration started at `(30,30)px`
> - stride = `14`
> - number of nodes = `12100`
> - only full graph is available

#### For the pioneer 3AT

> The graphs for the pioneer robot contains `_pioneer_` in their filename.


![](heightmaps/quarry_cropped4_scaled2cm.png)
> - this is the heightmap from the quarry
> - image size = `1600x1600px`
> - size of the map = `30.4x30.4m`
> - resolution = `2cm` per pixel
> - max height = `10m`
> - patch size for estimation = `60x60px`
> - iteration started at `(30,30)px`
> - stride = `14`
> - number of nodes = `12100`
> - only full graph is available



### `graphs`

Contains the corresponding graphs. For a `512x512px` heightmap, a graph was created by fixing a `60x60px` patch and iterate over all the heightmap image with a variable stride. Iteration started at (30,30) and ends at (482,482). For each iterating patch a node was created with the corresponding iterating index ( from `0` to `~=((512-60)/stride)^2` ). For each patch, traversability was estimated with respect to its 8 neighbours. 

Two set of graphs:

- `_partial` without non-traversable edges (stride = `3`, 22500 nodes):

          * if the probability of traversing between two nodes is >0.5 there is an edge, else there is no edge.
          * these graphs do not contain `distance` information. Can be added if needed.

- `_full` with non-traversable edges (stride = `7`, 4096 nodes):

          * each node (except the ones at the heigthmap borders) is connected to its 8 neighbours.
          * stride was increased to avoid having a huge graph to search on. Can be decreased if needed.

#### visualization

Visualizing the graphs requires `graphviz`. This snippet displays the graph in a grid layout similar to the process used to iterate the heightmap (if you zoom out the layout resembles the heightmap):

```
dot t_graph_cnn_custom9_partial.dot -Tx11 -Kneato
```
> patience: this takes time given the large size of the graph

## graph tools

I used the `networkx` python library to create and manage the graph. It seems a good option to start using the data in the graphs. I recommend it. However, if you want to use Cpp there is `Boost::graph`.

This is an example on how to read the graph with `networkx`:
```
# import library (when installing remember also to install pygraphviz and graphviz)
import networkx as nx

# read graph from dot file
G = nx.drawing.nx_agraph.read_dot('t_graph_cnn_custom9_partial.dot')

# a shortest path example
print (nx.shortest_path (G, '0', '2000'))
```

## links

`networkx`:

- [installation (I suggest to use `pip` version) ](https://networkx.github.io/index.html)
- [examples](http://networkx.readthedocs.io/en/networkx-1.11/examples/)

`Boost:graph`:

- installation (http://www.boost.org/doc/libs/1_64_0/libs/graph/doc/index.html)
